export const reviews = [
    {
        id: '1',
        text: 'super libro',
        book: '1'
    },
    {
        id: '2',
        text: 'buen libro',
        book: '1'
    },
    {
        id: '3',
        text: 'libro interesante',
        book: '2'
    },
    {
        id: '4',
        text: 'libro fantastico',
        book: '3'
    },
    {
        id: '5',
        text: 'buen libro de fantacia',
        book: '4'
    },
]