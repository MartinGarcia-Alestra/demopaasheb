FROM node:alpine

WORKDIR /
COPY package*.json ./
COPY ./src ./src
COPY .babelrc ./

RUN npm install
ENV PORT="9000"
CMD [ "npm", "run", "dev" ]
